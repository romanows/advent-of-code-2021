//! Solution to [Advent of Code 2021 Day 4](https://adventofcode.com/2021/day/4).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day04.txt")[..] }; }

/// Bingo board.
#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct BingoBoard {
    num_rows: usize,
    num_cols: usize,
    entries: Vec<u8>,
    marked: Vec<bool>,
}

impl BingoBoard {
    /// Create a new bingo board
    fn new(num_rows: usize, num_cols: usize, entries: &[u8]) -> BingoBoard {
        let entries = entries.to_vec();
        let marked = vec![false; entries.len()];

        if num_rows != num_cols {
            panic!("non-square Bingo Board with {} rows and {} columns", num_rows, num_cols);
        }

        if num_rows * num_cols != entries.len() {
            panic!("ill-formed Bingo Board with {} rows, {} columns, but {} entries", num_rows, num_cols, entries.len());
        }

        BingoBoard { num_rows, num_cols, entries, marked }
    }

    /// Create a new bingo board from the AoC string representation
    fn parse(board_str: &str) -> Option<BingoBoard> {
        let mut num_rows: usize = 0;
        let mut entries: Vec<u8> = Vec::new();
        for row in board_str.split('\n') {
            if row.is_empty() {
                if entries.is_empty() {
                    continue;  // ignore leading newlines
                } else {
                    break;
                }
            }
            num_rows += 1;
            for col in row.split(' ').filter(|x| !x.is_empty()) {
                entries.push(col.parse::<u8>().unwrap());
            }
        }
        if entries.is_empty() {
            None
        } else {
            let num_cols = entries.len() / num_rows;
            Some(BingoBoard::new(num_rows, num_cols, &entries))
        }
    }

    /// Get the entry at a particular (row, column) address.
    #[allow(dead_code)]
    fn get(&self, row: usize, col: usize) -> u8 {
        self.entries[col + (row * self.num_cols)]
    }

    /// Mark the given entry (entries?) as being covered.
    fn mark(&mut self, entry: u8) {
        for (x, m) in self.entries.iter().zip(self.marked.iter_mut()) {
            if *x == entry {
                *m = true;
            }
        }
    }

    /// `true` if the board is in a winning state.
    fn is_winning(&self) -> bool {
        // Check for horizontal matches
        let mut is_winning =
            (0..self.marked.len()).step_by(self.num_cols)            // start at the beginning of each row
                .any(|row_idx| (row_idx..(row_idx + self.num_cols))  // scan across all columns in the row
                     .all(|col_idx| self.marked[col_idx]));          // check to see if all entries have been marked

        // Check for vertical matches
        is_winning |=
            (0..self.num_cols)                                                      // start at the beginning of each column
                .any(|col_idx| (col_idx..self.marked.len()).step_by(self.num_cols)  // scan across all rows along the column
                     .all(|row_idx| self.marked[row_idx]));                         // check to see if all entries have been marked
        is_winning
    }

    /// Sum of all unmarked numbers.
    fn score(&self) -> u32 {
        self.entries.iter().zip(self.marked.iter()).fold(0, |acc, (x, m)| { acc + if *m {0} else {*x as u32} })
    }

    /// Calculate the final score given a series of numbers.
    #[allow(dead_code)]
    fn mark_and_score_all(&mut self, entries: &[u8]) -> Option<u32> {
        for entry in entries {
            self.mark(*entry);
            if self.is_winning() {
                return Some(*entry as u32 * self.score());
            }
        }
        None  // board was not a winner
    }
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let mut lines_iter = f.lines().map(|x| x.unwrap());

    let entries: Vec<u8> = lines_iter.next().unwrap().split(',').map(|x| x.parse::<u8>().unwrap()).collect();
    lines_iter.next();  // skip whitespace following the entries

    let mut bingo_boards: Vec<BingoBoard> = Vec::new();
    let mut board_lines: Vec<String> = Vec::new();
    for line in lines_iter {
        if ! line.is_empty() {
            board_lines.push(line);
        } else if let Some(bingo_board) = BingoBoard::parse(&board_lines.join("\n")) {
            bingo_boards.push(bingo_board);
            board_lines = Vec::new();
        }
    }

    // Account for the last board if it had not been followed by an empty line
    if let Some(bingo_board) = BingoBoard::parse(&board_lines.join("\n")) {
        bingo_boards.push(bingo_board);
    }

    let mut ans_part_1: Option<u32> = None;  // first board to win
    let mut ans_part_2: Option<u32> = None;  // last board to win
    for entry in entries {
        for board in bingo_boards.iter_mut() {
            if ! board.is_winning() {
                board.mark(entry);
                if board.is_winning() {
                    let final_score = entry as u32 * board.score() as u32;
                    if ans_part_1.is_none() {
                        ans_part_1 = Some(final_score);
                    }
                    ans_part_2 = Some(final_score);
                }
            }
        }
    }

    Ok((ans_part_1.unwrap(), ans_part_2.unwrap()))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: first winning board has final score: {}", ans_part_1);
    println!("Part 2: last winning board has final score: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! include_sample_input_bytes { () => { &include_bytes!("../../data/day04.sample.txt")[..] }; }

    #[test]
    fn test_bingo_board_new() {
        let bingo_board_new = BingoBoard::new(5, 5, &[22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19]);
        assert_eq!(bingo_board_new.num_rows, 5);
        assert_eq!(bingo_board_new.num_cols, 5);
        assert_eq!(bingo_board_new.entries, vec![22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19]);
        assert_eq!(bingo_board_new.marked, vec![false; 25]);
    }

    #[test]
    fn test_bingo_board_parse() {
        let bingo_board_parsed = BingoBoard::parse("\n\n");
        assert_eq!(bingo_board_parsed, None);

        let bingo_board_parsed = BingoBoard::parse(concat![
            "22 13 17 11  0\n",
            " 8  2 23  4 24\n",
            "21  9 14 16  7\n",
            " 6 10  3 18  5\n",
            " 1 12 20 15 19\n",
        ]).unwrap();
        assert_eq!(bingo_board_parsed.num_rows, 5);
        assert_eq!(bingo_board_parsed.num_cols, 5);
        assert_eq!(bingo_board_parsed.entries, vec![22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19]);
        assert_eq!(bingo_board_parsed.marked, vec![false; 25]);
    }

    #[test]
    fn test_bingo_board_mark() {
        let mut bb = BingoBoard::new(2, 2, &[1, 2, 3, 4]);

        // Marking a number that isn't present has no effect
        bb.mark(5);
        assert_eq!(bb.marked, vec![false, false, false, false]);

        bb.mark(2);
        assert_eq!(bb.marked, vec![false, true, false, false]);

        // Repeating the marking doesn't have any effect
        bb.mark(2);
        assert_eq!(bb.marked, vec![false, true, false, false]);

        bb.mark(4);
        assert_eq!(bb.marked, vec![false, true, false, true]);

        bb.mark(1);
        assert_eq!(bb.marked, vec![true, true, false, true]);

        bb.mark(3);
        assert_eq!(bb.marked, vec![true, true, true, true]);

        bb.mark(5);
        assert_eq!(bb.marked, vec![true, true, true, true]);
    }

    #[test]
    fn test_bingo_board_is_winning() {
        // First horizontal win
        let mut bb = BingoBoard::new(3, 3, &[1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(bb.is_winning(), false);
        bb.mark(1);
        assert_eq!(bb.is_winning(), false);
        bb.mark(2);
        assert_eq!(bb.is_winning(), false);
        bb.mark(4);  // distractor
        assert_eq!(bb.is_winning(), false);
        bb.mark(3);
        assert_eq!(bb.is_winning(), true);

        // Last horizontal win
        let mut bb = BingoBoard::new(3, 3, &[1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(bb.is_winning(), false);
        bb.mark(7);
        assert_eq!(bb.is_winning(), false);
        bb.mark(8);
        assert_eq!(bb.is_winning(), false);
        bb.mark(4);  // distractor
        assert_eq!(bb.is_winning(), false);
        bb.mark(9);
        assert_eq!(bb.is_winning(), true);

        // First vertical win
        let mut bb = BingoBoard::new(3, 3, &[1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(bb.is_winning(), false);
        bb.mark(1);
        assert_eq!(bb.is_winning(), false);
        bb.mark(4);
        assert_eq!(bb.is_winning(), false);
        bb.mark(3);  // distractor
        assert_eq!(bb.is_winning(), false);
        bb.mark(7);
        assert_eq!(bb.is_winning(), true);

        // Last vertical win
        let mut bb = BingoBoard::new(3, 3, &[1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(bb.is_winning(), false);
        bb.mark(3);
        assert_eq!(bb.is_winning(), false);
        bb.mark(6);
        assert_eq!(bb.is_winning(), false);
        bb.mark(4);  // distractor
        assert_eq!(bb.is_winning(), false);
        bb.mark(9);
        assert_eq!(bb.is_winning(), true);

        // Both vertical and horizontal wins!
        let mut bb = BingoBoard::new(3, 3, &[1, 2, 3, 4, 5, 6, 7, 8, 9]);
        assert_eq!(bb.is_winning(), false);
        bb.mark(4);
        assert_eq!(bb.is_winning(), false);
        bb.mark(6);
        assert_eq!(bb.is_winning(), false);
        bb.mark(2);
        assert_eq!(bb.is_winning(), false);
        bb.mark(8);
        assert_eq!(bb.is_winning(), false);
        bb.mark(5);
        assert_eq!(bb.is_winning(), true);
    }

    #[test]
    fn test_bingo_board_score() {
        let mut bb = BingoBoard::new(5, 5, &[14, 21, 17, 24, 4, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, 7]);
        for m in [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24] {
            bb.mark(m);
        }
        assert_eq!(bb.score(), 188);
    }

    #[test]
    fn test_bingo_given_part1() {
        let mut bb = BingoBoard::new(5, 5, &[14, 21, 17, 24, 4, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, 7]);
        let entries = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3, 26, 1];
        assert_eq!(bb.mark_and_score_all(&entries).unwrap(), 4512);
    }

    #[test]
    fn test_bingo_givens_part1() {
        let f = Box::new(BufReader::new(include_sample_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 4512);
        assert_eq!(ans_part_2, 1924);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 71708);
        assert_eq!(ans_part_2, 34726);
    }
}
