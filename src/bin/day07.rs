//! Solution to [Advent of Code 2021 Day 7](https://adventofcode.com/2021/day/7).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day07.txt")[..] }; }

/// Reports the minimal amount of fuel needed to align all elements at their
/// starting `horizontal_positions`.
fn minimize_fuel_part1(horizontal_positions: &[u32]) -> u32 {
    let search_start: u32 = *horizontal_positions.iter().min().unwrap();
    let search_end: u32 = *horizontal_positions.iter().max().unwrap();
    (search_start..=search_end).map(|i| horizontal_positions.iter().map(|x| (*x as i32 - i as i32).abs() as u32).sum()).min().unwrap()
}

/// Reports the minimal amount of fuel needed to align all elements at their
/// starting `horizontal_positions`, taking into account the non-linear amount of
/// fuel required to move an increasing number of steps.
fn minimize_fuel_part2(horizontal_positions: &[u32]) -> u32 {
    let search_start: u32 = *horizontal_positions.iter().min().unwrap();
    let search_end: u32 = *horizontal_positions.iter().max().unwrap();

    fn cost(x: u32, i: u32) -> u32 {
        let c = (x as i32 - i as i32).abs() as u32;
        ((c + 1) * c) / 2  // sum_i^c c -- how much fuel is required to move c positions
    }

    (search_start..=search_end).map(|i| horizontal_positions.iter().map(|x| cost(*x, i)).sum()).min().unwrap()
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let horizontal_positions: Vec<u32> = f.lines().map(|x| x.unwrap()).next().unwrap().split(',').map(|x| x.parse::<u32>().unwrap()).collect();
    let ans_part_1 = minimize_fuel_part1(&horizontal_positions);
    let ans_part_2 = minimize_fuel_part2(&horizontal_positions);
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: minimum amount of fuel required to align: {}", ans_part_1);
    println!("Part 2: minimum amount of fuel required to align: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1_givens() {
        assert_eq!(minimize_fuel_part1(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 37);
    }

    #[test]
    fn test_part_2_givens() {
        assert_eq!(minimize_fuel_part2(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 168);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 344297);
        assert_eq!(ans_part_2, 97164301);
    }
}
