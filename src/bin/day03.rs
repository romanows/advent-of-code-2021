//! Solution to [Advent of Code 2021 Day 3](https://adventofcode.com/2021/day/3).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day03.txt")[..] }; }

/// Converts a string representing a binary value with "1" and "0" into a vec of
/// bits stored as 1_u16 and 0_u16.
fn to_bits_you_say(bitstring: &str) -> Vec<u16> {
    bitstring.chars().map(|x| x.to_digit(2).unwrap() as u16).collect()
}

/// Counts the number of "1"'s in each position of the binary numbers in the
/// diagnostic_report.
fn get_bit_counts(diagnostic_report: &[String]) -> Vec<u16> {
    let mut counts: Vec<u16> = Vec::new();
    for bitstring in diagnostic_report {
        let bits = to_bits_you_say(bitstring);
        if counts.is_empty() {
            counts.extend(bits.iter());
        } else {
            for (i, x) in bits.iter().enumerate() {
                if *x == 1 {
                    counts[i] += 1;
                }
            }
        }
    }
    counts
}

/// Creates a vec that stores the majority bit for a collection of binary numbers of
/// length `num_entries` whose bit counts are given by `counts`.
fn get_majority_bits(num_entries: u16, counts: &[u16]) -> Vec<u16> {
    let majority_threshold = num_entries / 2;  // not quite sure what we do in case of a tie?
    counts.iter().map(|x| if x > &(majority_threshold) {1} else {0}).collect()
}

/// Converts the individual bit entries in the given reversed vec to the integer
/// they represent.
fn convert_reversed_bits_to_int(bits: &[u16]) -> u16 {
    bits.iter().enumerate().fold(0, |acc, (i, x)| acc + x * 2_u16.pow(i.try_into().unwrap()))
}

/// Calculates either the oxygen or co2_sensor rating, recursively.
fn calc_rating(is_oxygen_calc: bool, diagnostic_report: &[&str], position: usize) -> String {
    // Divide the bitstrings in the diagnostic report into buckets according to the
    // bits at the given position. If we're looking for the oxygen reading, we go with
    // the majority (or 1s to break ties) bucket.
    //
    // We could have implemented this in-place by sorting slices of the Vec and passing
    // those along instead of creating new Vecs for each recursive call. Arguably we
    // should have parsed the bitsrings into more-easily-addressible bits (so we don't
    // need to iterate through the first n-1 characters to look at the nth character).
    // We could have returned a &str if we were willing to deal with lifetime
    // annotations.
    if diagnostic_report[0].len() <= position || diagnostic_report.len() == 1 {
        return diagnostic_report[0].to_string()
    }
    let mut zeros: Vec<&str> = Vec::new();
    let mut ones: Vec<&str> = Vec::new();
    for bitstring in diagnostic_report {
        if bitstring.chars().nth(position).unwrap() == '0' {
            zeros.push(bitstring);
        } else {
            ones.push(bitstring);
        }
    }
    if zeros.len() <= ones.len() {
        calc_rating(is_oxygen_calc, if is_oxygen_calc {&ones} else {&zeros}, position + 1)
    } else {
        calc_rating(is_oxygen_calc, if is_oxygen_calc {&zeros} else {&ones}, position + 1)
    }
}

/// Calculates the oxygen and co2_scrubber ratings.
fn calc_sensor_readings(diagnostic_report: &[String]) -> (u16, u16) {
    let diagnostic_report_str: Vec<&str> = diagnostic_report.iter().map(|x| x.as_str()).collect();

    let oxygen_rating_bitstring = calc_rating(true, &diagnostic_report_str, 0);
    let mut oxygen_rating_bits = to_bits_you_say(&oxygen_rating_bitstring);
    oxygen_rating_bits.reverse();
    let oxygen_rating = convert_reversed_bits_to_int(&oxygen_rating_bits);

    let co2_scrubber_rating_bitstring = calc_rating(false, &diagnostic_report_str, 0);
    let mut co2_scrubber_rating_bits = to_bits_you_say(&co2_scrubber_rating_bitstring);
    co2_scrubber_rating_bits.reverse();
    let co2_scrubber_rating = convert_reversed_bits_to_int(&co2_scrubber_rating_bits);

    (oxygen_rating, co2_scrubber_rating)
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
#[allow(clippy::type_complexity)]
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<((u16, u16, u32), (u16, u16, u32))> {
    let diagnostic_report: Vec<String> = f.lines().map(|x| x.unwrap()).collect();

    let counts = get_bit_counts(&diagnostic_report);
    let mut majority_bits = get_majority_bits(diagnostic_report.len().try_into().unwrap(), &counts);
    majority_bits.reverse();
    let gamma = convert_reversed_bits_to_int(&majority_bits);
    let epsilon = gamma ^ (2_u16.pow(majority_bits.len().try_into().unwrap()) - 1);
    let ans_part_1 = gamma as u32 * epsilon as u32;

    let (oxygen_rating, co2_scrubber_rating) = calc_sensor_readings(&diagnostic_report);
    let ans_part_2 = oxygen_rating as u32 * co2_scrubber_rating as u32;

    Ok(((gamma, epsilon, ans_part_1), (oxygen_rating, co2_scrubber_rating, ans_part_2)))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let ((part_1_gamma, part_1_epsilon, ans_part_1), (oxygen_rating, co2_scrubber_rating, ans_part_2)) = main_quiet(f)?;
    println!("Part 1: gamma is {} and epsilon is {}, so power consumption of the submarine is: {}", part_1_gamma, part_1_epsilon, ans_part_1);
    println!("Part 2: oxygen generator rating is {} and CO2 scrubber rating is {}, so the life support rating of the submarine is: {}", oxygen_rating, co2_scrubber_rating, ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_bit_counts() {
        assert_eq!(get_bit_counts(&vec!["0".to_string()]), vec![0]);
        assert_eq!(get_bit_counts(&vec!["1".to_string()]), vec![1]);

        assert_eq!(get_bit_counts(&vec!["00".to_string()]), vec![0, 0]);
        assert_eq!(get_bit_counts(&vec!["01".to_string()]), vec![0, 1]);
        assert_eq!(get_bit_counts(&vec!["10".to_string()]), vec![1, 0]);
        assert_eq!(get_bit_counts(&vec!["11".to_string()]), vec![1, 1]);

        assert_eq!(get_bit_counts(&vec!["00".to_string(), "01".to_string(), "11".to_string()]), vec![1, 2]);

        // From the part 1 example
        assert_eq!(get_bit_counts(&vec![
            "00100".to_string(),
            "11110".to_string(),
            "10110".to_string(),
            "10111".to_string(),
            "10101".to_string(),
            "01111".to_string(),
            "00111".to_string(),
            "11100".to_string(),
            "10000".to_string(),
            "11001".to_string(),
            "00010".to_string(),
            "01010".to_string(),
        ]), vec![7, 5, 8, 7, 5]);
    }

    #[test]
    fn test_get_majority_bits() {
        assert_eq!(get_majority_bits(3, &vec![0]), vec![0]);
        assert_eq!(get_majority_bits(3, &vec![1]), vec![0]);
        assert_eq!(get_majority_bits(3, &vec![2]), vec![1]);
        assert_eq!(get_majority_bits(3, &vec![3]), vec![1]);

        // From the part 1 example
        assert_eq!(get_majority_bits(12, &vec![7, 5, 8, 7, 5]), vec![1, 0, 1, 1, 0]);
    }

    #[test]
    fn test_convert_reversed_bits_to_int() {
        assert_eq!(convert_reversed_bits_to_int(&[0]), 0);
        assert_eq!(convert_reversed_bits_to_int(&[1]), 1);
        assert_eq!(convert_reversed_bits_to_int(&[0, 0]), 0);
        assert_eq!(convert_reversed_bits_to_int(&[1, 0]), 1);  // remember the arg is reversed, so we're converting "01"
        assert_eq!(convert_reversed_bits_to_int(&[0, 1]), 2);
        assert_eq!(convert_reversed_bits_to_int(&[1, 1]), 3);

        // From the part 1 example
        assert_eq!(convert_reversed_bits_to_int(&[0, 1, 1, 0, 1]), 22);
    }

    #[test]
    fn test_given_part_2() {
        let diagnostic_report = vec![
            "00100".to_string(),
            "11110".to_string(),
            "10110".to_string(),
            "10111".to_string(),
            "10101".to_string(),
            "01111".to_string(),
            "00111".to_string(),
            "11100".to_string(),
            "10000".to_string(),
            "11001".to_string(),
            "00010".to_string(),
            "01010".to_string(),
        ];
        let (oxygen_rating, co2_scrubber_rating) = calc_sensor_readings(&diagnostic_report);
        assert_eq!(oxygen_rating, 23);
        assert_eq!(co2_scrubber_rating, 10);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let ((_, _, ans_part_1), (_, _, ans_part_2)) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 2003336);
        assert_eq!(ans_part_2, 1877139);
    }
}
