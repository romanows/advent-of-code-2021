//! Solution to [Advent of Code 2021 Day 6](https://adventofcode.com/2021/day/6).
//!
//! I did notice a couple of headlines on [reddit](https://www.reddit.com/r/adventofcode)
//! that mentioned the exponential explosion in part 2 of this problem. I'm not sure
//! what it'll look like exactly since I'm just starting part 1 as of writing this,
//! but let's take a bit more care to design an efficient-enough solution.
//!
//! Idea is to have a circular buffer that is sized according to the steady-state
//! countdown to reproduction. The current position holds all lanternfish that are
//! at day 0 of their reproductive cycle. The next cell holds all lanternfish that
//! are at day 1, etc., through day 6.
//!
//! When the lanternfish reproduce, their offspring have to wait a bit to reach
//! their steady state, so we'll have an extra buffer for the 8 and 7 day baby
//! lanternfish.
//!
//! The sum of all cells in these two arrays represent the number of lanternfish we
//! care about through the current day.
//!
//! To know how many lanternfish there will be tomorrow, we'll take a step. All the
//! lanternfish at the current position (day 0) reproduce. We add their offspring
//! into the special array for day 8. Day 8 moves to day 7, day 7 moves to the
//! circular buffer for day 6 (our current position). And then we increment our
//! current position to signify that we're on the next day (those lanternfish who
//! were previously at day 1).
//!
//! Take as many steps as there are days to consider. This should be relatively
//! efficient because it's linear with the number of days.

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day06.txt")[..] }; }

/// Calculate the number of Lanternfish after `num_days` given the initial
/// `times_to_reproduction`.
fn get_num_lanternfish(times_to_reproduction: &[u8], num_days: u16) -> u64 {
    let mut circular_buffer: [u64; 7] = [0; 7];
    let mut circular_buffer_idx: usize = 0;

    let mut day_8: u64 = 0;
    let mut day_7: u64 = 0;

    // Collapse the input into our circular buffer
    for x in times_to_reproduction {
        circular_buffer[*x as usize] += 1;
    }

    // Reproduce for the given number of days
    for _ in 0..num_days {
        let num_day_0 = circular_buffer[circular_buffer_idx];
        circular_buffer[circular_buffer_idx] += day_7;
        day_7 = day_8;
        day_8 = num_day_0;

        circular_buffer_idx = (circular_buffer_idx + 1) % 7;
    }

    circular_buffer.iter().sum::<u64>() + day_7 + day_8
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u64, u64)> {
    let times_to_reproduction: Vec<u8> = f.lines().map(|x| x.unwrap()).next().unwrap().split(',').map(|x| x.parse::<u8>().unwrap()).collect();
    let ans_part_1: u64 = get_num_lanternfish(&times_to_reproduction, 80);
    let ans_part_2: u64 = get_num_lanternfish(&times_to_reproduction, 256);
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: number of lanternfish after 80 days: {}", ans_part_1);
    println!("Part 2: number of lanternfish after 256 days: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1_givens() {
        assert_eq!(get_num_lanternfish(&[3, 4, 3, 1, 2], 18), 26);
        assert_eq!(get_num_lanternfish(&[3, 4, 3, 1, 2], 80), 5934);
    }

    #[test]
    fn test_part_2_givens() {
        assert_eq!(get_num_lanternfish(&[3, 4, 3, 1, 2], 256), 26984457539);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 356190);
        assert_eq!(ans_part_2, 1617359101538);
    }
}
