//! Solution to [Advent of Code 2021 Day 2](https://adventofcode.com/2021/day/2).
//!
//! Cribbed parts of the input parsing code from my [AoC2019 Day 3 solution](https://gitlab.com/romanows/advent-of-code-2019/-/blob/master/src/bin/day03.rs).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day02.txt")[..] }; }

/// Position in the sea
#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Position {
    horiz: i32,  // horizontal position
    depth: i32,
    aim: i32,
}

impl Position {
    fn new(horiz: i32, depth: i32, aim: i32) -> Position {
        Position { horiz, depth, aim }
    }
}

/// Submarine movement command
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
struct Command {
    dir: Direction,
    value: u16
}

impl Command {
    #[cfg(test)]
    fn new(dir: Direction, value: u16) -> Command {
        Command { dir, value }
    }

    /// Parse one command of the form "<direction> <value>".
    fn parse(s: &str) -> Command {
        let get_value = |x: &str| s[(x.len() + 1)..].parse::<u16>().unwrap();  // +1 due to the space character after the command
        let (dir, value) = match &s {
            _ if s.starts_with("up") => (Direction::Up, get_value("up")),
            _ if s.starts_with("down") => (Direction::Down, get_value("down")),
            _ if s.starts_with("forward") => (Direction::Forward, get_value("forward")),
            e => panic!("Illegal direction '{}'", e),
        };
        Command { dir, value }
    }

    /// Applies this command to the given position, returning the resulting new
    /// position. Ignores the "aim" component introduced in Part 2.
    fn exec_noaim(&self, p: Position) -> Position {
        match self.dir {
            Direction::Up => Position::new(p.horiz, p.depth - self.value as i32, 0),
            Direction::Down => Position::new(p.horiz, p.depth + self.value as i32, 0),
            Direction::Forward => Position::new(p.horiz + self.value as i32, p.depth, 0),
        }
    }

    /// Applies this command to the given position, returning the resulting new
    /// position.
    fn exec(&self, p: Position) -> Position {
        match self.dir {
            Direction::Up => Position::new(p.horiz, p.depth, p.aim - self.value as i32),
            Direction::Down => Position::new(p.horiz, p.depth, p.aim + self.value as i32),
            Direction::Forward => Position::new(p.horiz + self.value as i32, p.depth + p.aim * self.value as i32, p.aim),
        }
    }
}

/// Submarine movement direction, used as part of a `Command`.
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
enum Direction {Up, Down, Forward}

/// Incorrectly gets the final position of the submarine having starting at `Position(0, 0)` and
/// after following the given `commands`.
///
/// Incorrect because it uses the incorrect command execution formula from part 1
/// that ignores the `aim` component of the sub's position.
fn get_final_position_noaim(commands: &[Command]) -> Position {
    commands.iter().fold(Position::new(0, 0, 0), |acc, command| command.exec_noaim(acc))
}

/// Gets the final position of the submarine having starting at `Position(0, 0)` and
/// after following the given `commands`.
fn get_final_position(commands: &[Command]) -> Position {
    commands.iter().fold(Position::new(0, 0, 0), |acc, command| command.exec(acc))
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<((Position, i32), (Position, i32))> {
    let commands: Vec<Command> = f.lines().map(|x| Command::parse(&x.unwrap())).collect();

    let part_1_final_position: Position = get_final_position_noaim(&commands);
    let ans_part_1 = part_1_final_position.horiz * part_1_final_position.depth;

    let part_2_final_position: Position = get_final_position(&commands);
    let ans_part_2 = part_2_final_position.horiz * part_2_final_position.depth;

    Ok(((part_1_final_position, ans_part_1), (part_2_final_position, ans_part_2)))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let ((part_1_final_position, ans_part_1), (part_2_final_position, ans_part_2)) = main_quiet(f)?;
    println!("Part 1: Final horizontal position {} multiplied by the final depth {} is: {}", part_1_final_position.horiz, part_1_final_position.depth, ans_part_1);
    println!("Part 2: Final horizontal position {} multiplied by the final depth {} is: {}", part_2_final_position.horiz, part_2_final_position.depth, ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_command_parse() {
        assert_eq!(Command::parse("forward 5"), Command::new(Direction::Forward, 5));
        assert_eq!(Command::parse("down 8"), Command::new(Direction::Down, 8));
        assert_eq!(Command::parse("up 3"), Command::new(Direction::Up, 3));
    }

    #[test]
    fn test_given_part1_get_final_position_noaim() {
        let commands = vec![
            Command::new(Direction::Forward, 5),
            Command::new(Direction::Down, 5),
            Command::new(Direction::Forward, 8),
            Command::new(Direction::Up, 3),
            Command::new(Direction::Down, 8),
            Command::new(Direction::Forward, 2),
        ];
        assert_eq!(get_final_position_noaim(&commands), Position::new(15, 10, 0));
    }

    #[test]
    fn test_extra_part1() {
        assert_eq!(get_final_position_noaim(&vec![]), Position::new(0, 0, 0));  // sub remains at start when there are no movement commands
    }

    #[test]
    fn test_given_part2_get_final_position() {
        let commands = vec![
            Command::new(Direction::Forward, 5),
            Command::new(Direction::Down, 5),
            Command::new(Direction::Forward, 8),
            Command::new(Direction::Up, 3),
            Command::new(Direction::Down, 8),
            Command::new(Direction::Forward, 2),
        ];
        assert_eq!(get_final_position(&commands), Position::new(15, 60, 10));
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let ((_, ans_part_1), (_, ans_part_2)) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 1499229);
        assert_eq!(ans_part_2, 1340836560);
    }
}
