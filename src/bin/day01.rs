//! Solution to [Advent of Code 2021 Day 1](https://adventofcode.com/2021/day/1).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day01.txt")[..] }; }

/// Return the number of increases in a sequence of depth readings.
fn get_num_increases(depths: &[u32]) -> u32 {
    depths.windows(2).fold(0, |acc, p| if let [x, y] = p {acc + if x < y {1} else {0}} else {acc})
}

/// Return the number of window'd increases in a sequence of depth readings, where
/// the window is size=3 and moves with stride=1.
fn get_num_window_increases(depths: &[u32]) -> u32 {
    depths.windows(4).fold(0, |acc, p| if let [x, y, z, a] = p {acc + if (x + y + z) < (y + z + a) {1} else {0}} else {acc})
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let depths: Vec<u32> = f.lines().map(|x| x.unwrap().parse::<u32>().unwrap()).collect();
    let ans_part_1 = get_num_increases(&depths);
    let ans_part_2 = get_num_window_increases(&depths);
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: Number of depth measurement increases: {}", ans_part_1);
    println!("Part 2: Number of depth measurement increases when smoothing window width=3, stride=1: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_part1() {
        assert_eq!(get_num_increases(&vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263]), 7);
    }

    #[test]
    fn test_extra_part1() {
        assert_eq!(get_num_increases(&vec![]), 0);
        assert_eq!(get_num_increases(&vec![1]), 0);
        assert_eq!(get_num_increases(&vec![1, 1]), 0);

        assert_eq!(get_num_increases(&vec![1, 2]), 1);
        assert_eq!(get_num_increases(&vec![2, 1]), 0);
    }

    #[test]
    fn test_given_part2() {
        assert_eq!(get_num_window_increases(&vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263]), 5);
    }

    #[test]
    fn test_extra_part2() {
        assert_eq!(get_num_window_increases(&vec![]), 0);
        assert_eq!(get_num_window_increases(&vec![1]), 0);
        assert_eq!(get_num_window_increases(&vec![1, 2]), 0);
        assert_eq!(get_num_window_increases(&vec![1, 2, 3]), 0);
        assert_eq!(get_num_window_increases(&vec![1, 2, 3, 4]), 1);  // finally, because (1 + 2 + 3) < (2 + 3 + 4)
        assert_eq!(get_num_window_increases(&vec![1, 2, 3, 1]), 0);  // no, because (1 + 2 + 3) == (2 + 3 + 1)
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        assert_eq!(main_quiet(f).unwrap(), (1564, 1611));
    }
}
