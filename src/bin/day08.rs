//! Solution to [Advent of Code 2021 Day 8](https://adventofcode.com/2021/day/8).
//!
//! There are displays in the sub that each have four 7-segment digits. For each
//! display, the connections from the computer to the digits are randomly shuffled.
//! The connections within a display are the same but differ between displays.
//!
//! The problem input are lines of observations, each line corresponding to a
//! different display (and thus to a potentially different mapping from signals to
//! segments in each line). The first part of each line is a recording of the
//! signals observed while the computer sends signals to display the digits 0
//! through 9 in a random order. Each digit appears only once. The second part of
//! each line are the signals that are intended to be the output of the display.

use std::collections::HashSet;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day08.txt")[..] }; }

/// Parsed observation for one display, corresponding to an entry/line in the
/// problem input.
#[derive(Debug, Eq, PartialEq, Clone)]
struct Entry {
    /// LHS of each line, these will map to the digits 0-9, albeit in a randomized order
    probes: Vec<HashSet<char>>,

    /// RHS of each line, these are the signals corresponding to the displayed digits
    digits: Vec<HashSet<char>>
}

impl Entry {
    fn parse(line: &str) -> Entry {
        let mut probes: Vec<HashSet<char>> = Vec::new();
        let mut digits: Vec<HashSet<char>> = Vec::new();
        let mut is_parsing_probes = true;
        for token in line.split(' ') {
            if token == "|" {
                is_parsing_probes = false;
                continue
            }
            let signal_set = token.chars().collect();
            if is_parsing_probes {
                probes.push(signal_set);
            } else {
                digits.push(signal_set);
            }
        }
        Entry { probes, digits }
    }
}

/// Counts the number of digits in the problem input with 2, 3, 4, or 7 segments
/// activated. This is purely to solve part 1 of the problem so I can see what part
/// 2 looks like.
fn count_part1_digits(entry: &Entry) -> u32 {
    entry.digits.iter().filter(|x| matches!(x.len(), 2 | 3 | 4 | 7)).count() as u32
}

/// Given the 0 through 9 "probe" signals for each display / entry, determines which
/// digits were intended to be shown on the display.
///
/// Since this is an AoC solution, I'll put some implementation details here so that
/// they'll show up in the generated documentation.
///
/// One could simply try all permutations of signals to segments to see what is
/// consistent with the 0-9 probe signals.  Not very elegant.
///
/// Instead, we start from a set of trivially identifiable signal groups for the
/// digits 1, 4, 7, and 8. We can also identify the set of signal groups that cover
/// the digits 2, 3, and 5. Initially these are ambiguous. Yet with this
/// information, we can build signal patterns for all digits through intersection,
/// union, and symmetric_difference (XOR) operations.
fn deduce_digits(entry: &Entry) -> u32 {
    // N00b in Rust here, having a lot of trouble with what would be relatively easy
    // set operations in another language. Bitfields would have been easier but I
    // wonder if there's a nicer way to do all this, especially without the intermediate
    // copying and collection calls "collect()" calls later on.

    // 1 uses two active signals
    let one:   &HashSet<char> = entry.probes.iter().find(|x| x.len() == 2).unwrap();
    let four:  &HashSet<char> = entry.probes.iter().find(|x| x.len() == 4).unwrap();
    let seven: &HashSet<char> = entry.probes.iter().find(|x| x.len() == 3).unwrap();
    let eight: &HashSet<char> = entry.probes.iter().find(|x| x.len() == 7).unwrap();

    // 2, 3, and 5 all use 5 active signals, although we can't tell which are which yet
    let two_three_five: Vec<&HashSet<char>> = entry.probes.iter().filter(|x| x.len() == 5).collect();

    // 2 and 5 are the only pair in `two_three_five` that differ by two active signals
    let mut two_five: Vec<&HashSet<char>> = Vec::new();
    if two_three_five[0].difference(two_three_five[1]).count() == 2 {
        two_five.push(two_three_five[0]);
        two_five.push(two_three_five[1]);
    } else if two_three_five[0].difference(two_three_five[2]).count() == 2 {
        two_five.push(two_three_five[0]);
        two_five.push(two_three_five[2]);
    } else {
        two_five.push(two_three_five[1]);
        two_five.push(two_three_five[2]);
    }

    // Signals that map to the three horizontal bars in the 7-segment display
    let adg: HashSet<char> =
        two_three_five[0].intersection(two_three_five[1]).copied().collect::<HashSet<char>>()
        .intersection(two_three_five[2]).copied().collect();

    // From this point on, we can build the remaining unknown digits by intersecting,
    // union, and XOR operations on patterns we've already identified.

    // 3 is the union of the horizontal segments and the vertical segments supplied by "1"
    let three: HashSet<char> = adg.union(one).copied().collect();

    let nine: HashSet<char> =
        seven.union(four).copied().collect::<HashSet<char>>()  // most of the nine
        .union(&adg).copied().collect();                       // adds in the bottom segment

    // second set corresponds to the .
    let zero: HashSet<char> =
        nine.symmetric_difference(four).copied().collect::<HashSet<char>>()                                             // top and bottom segments
        .union(&two_five[0].symmetric_difference(two_five[1]).copied().collect::<HashSet<char>>()).copied().collect();  // left and right sides

    // 6 is the remaining 6-segment signal
    let six: &HashSet<char> = entry.probes.iter().find(|x| x.len() == 6 && **x != zero && **x != nine).unwrap();

    let two: HashSet<char> = six.symmetric_difference(&nine).copied().collect::<HashSet<char>>().union(&adg).copied().collect();

    // Don't actually need to determine this but I'll leave it here for completeness'
    // sake. We know that if a signal group doesn't match any of the other digits, it
    // must be a 5.
    let _five: &HashSet<char> = *two_three_five.iter().find(|x| ***x != two && ***x != three).unwrap();

    let mut digits: Vec<u32> = entry.digits.iter().map(|x|
        if x == &zero       {0}
        else if x == one    {1}
        else if x == &two   {2}
        else if x == &three {3}
        else if x == four   {4}
        else if x == six    {6}
        else if x == seven  {7}
        else if x == eight  {8}
        else if x == &nine  {9}
        else                {5}).collect();

    // Covert digits to a number
    digits.reverse();
    digits.iter().enumerate().map(|(i, x)| x * 10_u32.pow(i as u32)).sum::<u32>()
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let entries: Vec<Entry> = f.lines().map(|x| Entry::parse(&x.unwrap())).collect();
    let ans_part_1 = entries.iter().map(|x| count_part1_digits(x)).sum();
    let ans_part_2 = entries.iter().map(|x| deduce_digits(x)).sum();
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: number of times that the digits with 2, 3, 4, or 7 segments appear: {}", ans_part_1);
    println!("Part 2: sum of the digits for each entry: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_part1_digits() {
        // From the problem description
        assert_eq!(count_part1_digits(&Entry::parse("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")), 0);
        assert_eq!(count_part1_digits(&Entry::parse("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")), 2);

        // All possible strings from length 1 to 8, we're only matching four
        assert_eq!(count_part1_digits(&Entry::parse("| a ab abc abcd abcde abcdef abcdefg abcdefgh")), 4);

        // Repeated with one distrctor
        assert_eq!(count_part1_digits(&Entry::parse("| ab ab ab ab a ab")), 5);
    }

    #[test]
    fn test_deduce_digits_part2_givens() {
        assert_eq!(deduce_digits(&Entry::parse("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")), 5353);
        assert_eq!(deduce_digits(&Entry::parse("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")), 8394);
        assert_eq!(deduce_digits(&Entry::parse("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc")), 9781);
        assert_eq!(deduce_digits(&Entry::parse("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg")), 1197);
        assert_eq!(deduce_digits(&Entry::parse("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb")), 9361);
        assert_eq!(deduce_digits(&Entry::parse("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea")), 4873);
        assert_eq!(deduce_digits(&Entry::parse("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb")), 8418);
        assert_eq!(deduce_digits(&Entry::parse("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe")), 4548);
        assert_eq!(deduce_digits(&Entry::parse("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef")), 1625);
        assert_eq!(deduce_digits(&Entry::parse("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb")), 8717);
        assert_eq!(deduce_digits(&Entry::parse("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")), 4315);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 456);
        assert_eq!(ans_part_2, 1091609);
    }
}
