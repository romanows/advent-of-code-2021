//! Solution to [Advent of Code 2021 Day 10](https://adventofcode.com/2021/day/10).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day10.txt")[..] }; }

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u64, u64)> {
    let mut ans_part_1 = 0;
    let mut ans_part_2: Vec<u64> = Vec::new();
    for line in f.lines() {
        let mut symbol_filo: Vec<char> = Vec::new();
        let mut is_corrupted: bool = false;
        for c in line.unwrap().chars() {
            match c {
                '(' | '[' | '{' | '<' => symbol_filo.push(c),
                ')' => if symbol_filo.pop() != Some('(') {ans_part_1 += 3; is_corrupted = true; break;},
                ']' => if symbol_filo.pop() != Some('[') {ans_part_1 += 57; is_corrupted = true; break;},
                '}' => if symbol_filo.pop() != Some('{') {ans_part_1 += 1197; is_corrupted = true; break;},
                '>' => if symbol_filo.pop() != Some('<') {ans_part_1 += 25137; is_corrupted = true; break;},
                _ => panic!("unexpected character: '{}'", c)
            }
        }
        if !is_corrupted {
            let mut autocorrect_score: u64 = 0;
            while !symbol_filo.is_empty() {
                match symbol_filo.pop() {
                    Some('(') => autocorrect_score = 5 * autocorrect_score + 1,
                    Some('[') => autocorrect_score = 5 * autocorrect_score + 2,
                    Some('{') => autocorrect_score = 5 * autocorrect_score + 3,
                    Some('<') => autocorrect_score = 5 * autocorrect_score + 4,
                    Some(c) => panic!("unexpected character: '{}'", c),
                    None => panic!("should never happen")
                }
            }
            if autocorrect_score != 0 {
                ans_part_2.push(autocorrect_score);
            }
        }
    };
    ans_part_2.sort_unstable();
    let ans_part_2 = if ans_part_2.is_empty() {0} else {ans_part_2[ans_part_2.len() / 2]};
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: total syntax error score: {}", ans_part_1);
    println!("Part 2: middle autocomplete score: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1_givens() {
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new("{([(<{}[<>[]}>{[]{[(<()>".as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 1197);

        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new("[[<[([]))<([[{}[[()]]]".as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 3);

        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new("[{[{({}]{}}([{[{{{}}([]".as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 57);

        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new("<{([([[(<>()){}]>(<<{{".as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 25137);

        let part_1_given = concat!(
            "{([(<{}[<>[]}>{[]{[(<()>\n",
            "[[<[([]))<([[{}[[()]]]\n",
            "[{[{({}]{}}([{[{{{}}([]\n",
            "[<(<(<(<{}))><([]([]()\n",
            "<{([([[(<>()){}]>(<<{{");
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(part_1_given.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 26397);
    }

    #[test]
    fn test_part_2_givens() {
        let givens = concat!(
            "[({(<(())[]>[[{[]{<()<>>\n",
            "[(()[<>])]({[<{<<[]>>(\n",
            "{([(<{}[<>[]}>{[]{[(<()>\n",
            "(((({<>}<{<{<>}{[]{[]{}\n",
            "[[<[([]))<([[{}[[()]]]\n",
            "[{[{({}]{}}([{[{{{}}([]\n",
            "{<[[]]>}<{[{[{[]{()[[[]\n",
            "[<(<(<(<{}))><([]([]()\n",
            "<{([([[(<>()){}]>(<<{{\n",
            "<{([{{}}[<[[[<>{}]]]>[]]\n");
        let (_, ans_part_2) = main_quiet(Box::new(BufReader::new(givens.as_bytes()))).unwrap();
        assert_eq!(ans_part_2, 288957);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, _) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 374061);
    }
}
