//! Solution to [Advent of Code 2021 Day 11](https://adventofcode.com/2021/day/11).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day11.txt")[..] }; }

/// Holds the grid of dumbo octopuses / counters.
#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct Grid {
    num_rows: usize,
    num_cols: usize,
    entries: Vec<u8>,
}

impl Grid {
    /// Parses the newline-delimited rows of 0-9 digits into a new Grid struct.
    fn parse(f: Box<dyn BufRead>) -> Grid {
        let mut num_rows: usize = 0;
        let mut entries: Vec<u8> = Vec::new();
        for line in f.lines().map(|x| x.unwrap()) {
            num_rows += 1;
            entries.extend(line.chars().map(|x| x.to_digit(10).unwrap() as u8));
        }
        let num_cols: usize = entries.len() / num_rows;
        Grid { num_rows, num_cols, entries }
    }

    /// Increment an entry to the point of pre-flash but no further.
    fn inc_to_flash(&mut self, idx: usize) {
        if self.entries[idx] < 10 {
            self.entries[idx] += 1;
        }
    }

    /// Increment an entry post-flash, to indicate that a flash has occurred and cannot
    /// occur again.
    fn inc_after_flash(&mut self, idx: usize) {
        if self.entries[idx] == 10 {
            self.entries[idx] += 1;
        }
    }

    /// Get the indexes that surround the given index, writing them into the given
    /// buffer after clearing it.
    fn get_neighborhood(&self, idx: usize, neighborhood: &mut Vec<usize>) {
        neighborhood.clear();

        let is_row_above = self.num_cols <= idx;
        let is_row_below = idx + self.num_cols < self.entries.len();
        let is_col_before = idx % self.num_cols != 0;
        let is_col_after = (idx + 1) % self.num_cols != 0;

        if is_row_above {
            let idx_above = idx - self.num_cols;
            if is_col_before { neighborhood.push(idx_above - 1); }
            neighborhood.push(idx_above);
            if is_col_after { neighborhood.push(idx_above + 1); }
        }

        if is_col_before { neighborhood.push(idx - 1); }
        if is_col_after { neighborhood.push(idx + 1); }

        if is_row_below {
            let idx_below = idx + self.num_cols;
            if is_col_before { neighborhood.push(idx_below - 1); }
            neighborhood.push(idx_below);
            if is_col_after { neighborhood.push(idx_below + 1); }
        }
    }

    /// Runs one step of the simulation, returning the number of flashes.
    fn step(&mut self) -> u32 {
        let mut num_flashes: u32 = 0;

        // First, increment all entries
        self.entries.iter_mut().for_each(|x| *x += 1);  // same as inc_to_flash()

        // Then search for entries that equal 10 (the lowest threshold for a flash), count
        // their flash, increment them to 11 (to mark that they've flashed), and then
        // increment their neighborhood if the neighborhood is <=9.
        let mut neighborhood: Vec<usize> = Vec::with_capacity(8);
        loop {
            let mut is_flash_observed = false;

            for i in 0..self.entries.len() {
                if self.entries[i] == 10 {
                    // Deal with the flashing entry
                    num_flashes += 1;
                    is_flash_observed = true;
                    self.inc_after_flash(i);

                    // Deal with its neighborhood
                    self.get_neighborhood(i, &mut neighborhood);
                    neighborhood.iter().for_each(|&n| self.inc_to_flash(n));
                }
            }

            if !is_flash_observed {
                break;
            }
        }

        // Finally, for all entries >9 (there should be no 10s, only 11s), reset them to 0.
        self.entries.iter_mut().for_each(|x| if *x > 9 {*x = 0});

        num_flashes
    }

    /// Runs `num_steps` steps of the simulation, returning the accumulated number of
    /// flashes from all steps.
    fn step_many(&mut self, num_steps: usize) -> u32 {
        (0..num_steps).map(|_| self.step()).sum()
    }

    /// Steps until all octopuses flash, returning the step at which this occurred.
    fn step_until_all_flash(&mut self) -> u32 {
        let mut num_steps = 0;
        loop {
            let num_flashes = self.step();
            num_steps += 1;

            if num_flashes == self.entries.len() as u32 {
                break;
            }
        }
        num_steps
    }
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let grid = Grid::parse(f);

    let mut grid_part1 = grid.clone();
    let ans_part_1 = grid_part1.step_many(100);

    let mut grid_part2 = grid;
    let ans_part_2 = grid_part2.step_until_all_flash();

    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: total flashes after 100 steps: {}", ans_part_1);
    println!("Part 2: steps until all octopuses flash: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_grid_parse() {
        let input = "1";
        let grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        assert_eq!(grid.num_rows, 1);
        assert_eq!(grid.num_cols, 1);
        assert_eq!(grid.entries, &[1]);

        let input = "11111\n19991\n19191\n19991\n11111";
        let grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        assert_eq!(grid.num_rows, 5);
        assert_eq!(grid.num_cols, 5);
        assert_eq!(grid.entries, &[1, 1, 1, 1, 1, 1, 9, 9, 9, 1, 1, 9, 1, 9, 1, 1, 9, 9, 9, 1, 1, 1, 1, 1, 1]);

        // irregular and with a trailing newline
        let input = "11111\n19991\n19191\n19991\n";
        let grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        assert_eq!(grid.num_rows, 4);
        assert_eq!(grid.num_cols, 5);
        assert_eq!(grid.entries, &[1, 1, 1, 1, 1, 1, 9, 9, 9, 1, 1, 9, 1, 9, 1, 1, 9, 9, 9, 1]);
    }

    #[test]
    fn test_grid_step_part1_givens1() {
        let input = "11111\n19991\n19191\n19991\n11111";
        let mut grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        let num_flashes = grid.step();
        assert_eq!(num_flashes, 9);
        assert_eq!(grid.entries, &[3, 4, 5, 4, 3, 4, 0, 0, 0, 4, 5, 0, 0, 0, 5, 4, 0, 0, 0, 4, 3, 4, 5, 4, 3]);
    }

    #[test]
    fn test_grid_step_part1_givens2() {
        let input = "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526";
        let mut grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));

        let num_flashes = grid.step();
        assert_eq!(num_flashes, 0);

        let num_flashes = grid.step();

        let state_after_two_steps = "8807476555\n5089087054\n8597889608\n8485769600\n8700908800\n6600088989\n6800005943\n0000007456\n9000000876\n8700006848";
        let grid_after_two_steps = Grid::parse(Box::new(BufReader::new(state_after_two_steps.as_bytes())));

        assert_eq!(num_flashes, 35);
        assert_eq!(grid.entries, grid_after_two_steps.entries);

        // Again, but with step_many()
        let input = "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526";
        let mut grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        let num_flashes = grid.step_many(2);
        assert_eq!(num_flashes, 35);
        assert_eq!(grid.entries, grid_after_two_steps.entries);
    }

    #[test]
    fn test_grid_step_part2_givens2() {
        let input = "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526";
        let mut grid = Grid::parse(Box::new(BufReader::new(input.as_bytes())));
        let num_steps = grid.step_until_all_flash();
        assert_eq!(num_steps, 195);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 1620);
        assert_eq!(ans_part_2, 371);
    }
}
