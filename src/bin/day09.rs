//! Solution to [Advent of Code 2021 Day 9](https://adventofcode.com/2021/day/9).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day09.txt")[..] }; }

/// Debug helper to map a flat heightmap index into a (row, column) coordinate.
#[allow(dead_code)]
fn to_xy(num_cols: usize, idx: usize) -> (usize, usize) {
    (idx / num_cols, idx % num_cols)
}

/// Parse the buffer into a heightmap.
fn parse_heightmap(f: Box<dyn BufRead>) -> (usize, usize, Vec<u8>) {
    let mut num_rows: usize = 0;
    let mut heightmap: Vec<u8> = Vec::new();
    for line in f.lines().map(|x| x.unwrap()) {
        num_rows += 1;
        heightmap.extend(line.chars().map(|x| x.to_digit(10).unwrap() as u8));
    }
    let num_cols: usize = heightmap.len() / num_rows;
    (num_rows, num_cols, heightmap)
}

/// Find the lowpoints in the heightmap.
fn get_low_point_idxs(num_cols: usize, heightmap: &[u8]) -> Vec<usize> {
    let is_lower_than_above = |i: usize| i < num_cols || heightmap[i] < heightmap[i - num_cols];  // if we're either on the top row or we're lower than the height above us
    let is_lower_than_below = |i: usize| heightmap.len() <= (i + num_cols) || heightmap[i] < heightmap[i + num_cols];  // if we're either on the top row or we're lower than the height above us
    let is_lower_than_left  = |i: usize| i % num_cols == 0 || heightmap[i] < heightmap[i - 1];
    let is_lower_than_right = |i: usize| i + 1 >= heightmap.len() || i + 1 % num_cols == 0 || heightmap[i] < heightmap[i + 1];
    (0..heightmap.len()).filter(|&i| is_lower_than_above(i) && is_lower_than_below(i) && is_lower_than_left(i) && is_lower_than_right(i)).collect()
}

/// Caclulate the "total risk" of the heightmap; i.e., the answer to part 1.
fn get_total_risk(num_cols: usize, heightmap: &[u8]) -> u32 {
    let low_point_idxs: Vec<usize> = get_low_point_idxs(num_cols, heightmap);
    low_point_idxs.iter().map(|&i| (heightmap[i] as u32) + 1).sum()
}

/// Calculate the size of the various basins found in the given heightmap.
fn get_basin_sizes(num_cols: usize, heightmap: &[u8]) -> Vec<usize> {
    let low_point_idxs: Vec<usize> = get_low_point_idxs(num_cols, heightmap);

    // Approach here is to create a vec storing basin IDs that parallels heightmap. We
    // seed it with the low points. We also seed two queues where we'll look for
    // horizontal members of a basin and vertical members of a basin. When we find a
    // new basin member during, say, a horizontal search, we'll add it to the vertical
    // search queue.  When the queues are empty, we'll have found all basins.
    //
    // Then at the end, you can collect the number of members of the basins, take the
    // largest three, and then multiply their sizes.
    let mut horizontal_expansion_queue: Vec<usize> = low_point_idxs.to_vec();
    let mut vertical_expansion_queue: Vec<usize> = low_point_idxs.to_vec();

    // Initialize the basin IDs. The vector parallels the heightmap and its elements
    // are basin IDs, initialized according to the low points we've detected. The
    // heightmap index is used as the basin_id, it doesn't need to be contiguous.
    let mut basin_ids: Vec<Option<usize>> = (0..heightmap.len()).map(|i| if low_point_idxs.contains(&i) {Some(i)} else {None}).collect();

    while !horizontal_expansion_queue.is_empty() || !vertical_expansion_queue.is_empty() {
        while !horizontal_expansion_queue.is_empty() {
            let expansion_idx = horizontal_expansion_queue.pop().unwrap();
            let row_start_inclusive_idx: usize = (expansion_idx / num_cols) * num_cols;
            let row_end_exclusive_idx = row_start_inclusive_idx + num_cols;

            // Further break this down into leftward and rightward expansion. Rightward
            // expansion first:
            for i in (expansion_idx + 1)..row_end_exclusive_idx {
                if basin_ids[i].is_some() || heightmap[i] == 9 {
                    break;
                }
                basin_ids[i] = basin_ids[expansion_idx];
                vertical_expansion_queue.push(i);
            }

            // Leftward expansion
            if expansion_idx > 0 {
                for i in (row_start_inclusive_idx..expansion_idx).rev() {
                    if basin_ids[i].is_some() || heightmap[i] == 9 {
                        break;
                    }
                    basin_ids[i] = basin_ids[expansion_idx];
                    vertical_expansion_queue.push(i);
                }
            }
        }

        vertical_expansion_queue.sort_unstable();
        vertical_expansion_queue.dedup();

        while !vertical_expansion_queue.is_empty() {
            let expansion_idx = vertical_expansion_queue.pop().unwrap();
            let col_start_inclusive_idx: usize = expansion_idx % num_cols;

            // Further break this down into upward and downward expansion. Downward expansion
            // first:
            for i in ((expansion_idx + num_cols)..basin_ids.len()).step_by(num_cols) {
                if basin_ids[i].is_some() || heightmap[i] == 9 {
                    break;
                }
                basin_ids[i] = basin_ids[expansion_idx];
                horizontal_expansion_queue.push(i);
            }

            // Upward expansion
            if expansion_idx >= num_cols {
                for i in (col_start_inclusive_idx..expansion_idx).step_by(num_cols).rev() {
                    if basin_ids[i].is_some() || heightmap[i] == 9 {
                        break;
                    }
                    basin_ids[i] = basin_ids[expansion_idx];
                    horizontal_expansion_queue.push(i);
                }
            }
        }

        horizontal_expansion_queue.sort_unstable();
        horizontal_expansion_queue.dedup();
    }

    let mut basin_sizes: Vec<usize> = vec![0; heightmap.len()];
    for i in basin_ids.into_iter().flatten() {  // flatten() skips the None values
        basin_sizes[i] += 1;
    }
    basin_sizes.retain(|&x| x > 0);
    basin_sizes.sort_unstable();
    basin_sizes
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let (_, num_cols, heightmap) = parse_heightmap(f);
    let ans_part_1 = get_total_risk(num_cols, &heightmap);

    let basin_sizes = get_basin_sizes(num_cols, &heightmap);
    let ans_part_2 = basin_sizes.iter().rev().take(3).product::<usize>() as u32;

    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: sum of the low point risk levels: {}", ans_part_1);
    println!("Part 2: product of the largest three basin sizes: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let given_input = "1";  // all points, trivially
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 2);

        let given_input = "12";  // top-left
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 2);

        let given_input = "11";  // none, because each cell is not lower than the other cell
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 0);

        let given_input = "12\n23";  // top-left
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 2);

        let given_input = "12\n21";  // top-left and lower-right corners are low points
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 4);
    }

    #[test]
    fn test_part1_givens() {
        let given_input = "2199943210\n3987894921\n9856789892\n8767896789\n9899965678";
        let (ans_part_1, _) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 15);
    }

    #[test]
    fn test_get_basin_sizes() {
        let basin_sizes = get_basin_sizes(1, &[1]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 1);

        // Horizontal row, low point in first position
        let basin_sizes = get_basin_sizes(2, &[1, 2]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 2);

        // Horizontal row, low point in second position
        let basin_sizes = get_basin_sizes(2, &[2, 1]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 2);

        // Vertical column, low point in first position
        let basin_sizes = get_basin_sizes(1, &[1, 2]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 2);

        // Vertical column, low point in second position
        let basin_sizes = get_basin_sizes(1, &[2, 1]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 2);

        // 2x2 board
        let basin_sizes = get_basin_sizes(2, &[1, 2, 3, 4]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 4);

        // 2x2 board, heights of 9 aren't members of basins
        let basin_sizes = get_basin_sizes(2, &[1, 2, 3, 9]);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 3);

        // 2x2 board, heights of 9 aren't members of basins, so they bound the two lowpoints
        let basin_sizes = get_basin_sizes(2, &[1, 9, 9, 4]);
        assert_eq!(basin_sizes.len(), 2);
        assert_eq!(basin_sizes[0], 1);
        assert_eq!(basin_sizes[1], 1);
    }

    #[test]
    fn test_part2_givens_isolation() {
        // Bug detecting the basin size for one of the basins, isolating it in its own test
        let given_input = "9999999999\n9987899999\n9856789999\n8767899999\n9899999999";
        let (_, num_cols, heightmap) = parse_heightmap(Box::new(BufReader::new(given_input.as_bytes())));
        let basin_sizes = get_basin_sizes(num_cols, &heightmap);
        assert_eq!(basin_sizes.len(), 1);
        assert_eq!(basin_sizes[0], 14);
    }

    #[test]
    fn test_part2_givens() {
        let given_input = "2199943210\n3987894921\n9856789892\n8767896789\n9899965678";
        let (_, ans_part_2) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_2, 1134);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 562);
        assert_eq!(ans_part_2, 1076922);
    }
}
