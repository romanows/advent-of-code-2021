//! Solution to [Advent of Code 2021 Day 5](https://adventofcode.com/2021/day/5).

extern crate lazy_static;

use lazy_static::lazy_static;
use regex::Regex;
use std::cmp::max;
use std::collections::{HashMap, HashSet};
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};


/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day05.txt")[..] }; }


/// Point in a cartesian plane.
#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    /// Constructor.
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}


/// Line in a cartesian plane.
#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Line {
    p: Point,
    q: Point,
}

impl Line {
    /// Constructor.
    #[allow(dead_code)]
    fn new(p: Point, q: Point) -> Line {
        Line { p, q }
    }

    /// Parses strings of the form "0,9 -> 5,9" into a line like `Line { Point::new(0, 9), Point::new(5, 9) }`.
    fn parse(text: &str) -> Line {
        lazy_static! {
            static ref LINE_RE: Regex = Regex::new(r"^([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)$").unwrap();
        }
        let mut line: Option<Line> = None;
        for cap in LINE_RE.captures_iter(text) {
            let p = Point::new((&cap[1]).parse::<i32>().unwrap(), (&cap[2]).parse::<i32>().unwrap());
            let q = Point::new((&cap[3]).parse::<i32>().unwrap(), (&cap[4]).parse::<i32>().unwrap());
            line = Some(Line { p, q });
        }
        line.unwrap_or_else(|| panic!("failed to parse {} as a Line", text))
    }

    /// true if this line is horizontal.
    fn is_horizontal(&self) -> bool {
        self.p.y == self.q.y
    }

    /// true if this line is vertical.
    fn is_vertical(&self) -> bool {
        self.p.x == self.q.x
    }

    /// Get the set of points covered by this line.
    fn get_points(&self) -> HashSet<Point> {
        let step_x = match (self.p.x, self.q.x) {
            (a, b) if a < b => 1,
            (a, b) if a > b => -1,
            _ => 0,
        };
        let step_y = match (self.p.y, self.q.y) {
            (a, b) if a < b => 1,
            (a, b) if a > b => -1,
            _ => 0,
        };
        let num_steps = max(step_x * (self.q.x - self.p.x), step_y * (self.q.y - self.p.y));
        (0..=num_steps).map(|i| Point::new(self.p.x + i * step_x, self.p.y + i * step_y)).collect()
    }
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(u32, u32)> {
    let all_lines: Vec<Line> = f.lines().map(|x| x.unwrap()).map(|x| Line::parse(&x)).collect();
    let straight_lines: Vec<Line> = all_lines.iter().filter(|x| x.is_horizontal() || x.is_vertical()).copied().collect();

    let mut straight_line_points_counter: HashMap<Point, u32> = HashMap::new();
    straight_lines.iter().flat_map(|x| x.get_points()).for_each(|x| *straight_line_points_counter.entry(x).or_insert(0) += 1);
    let ans_part_1: u32 = straight_line_points_counter.values().fold(0, |acc, x| acc + if *x > 1 {1} else {0});

    let mut all_line_points_counter: HashMap<Point, u32> = HashMap::new();
    all_lines.iter().flat_map(|x| x.get_points()).for_each(|x| *all_line_points_counter.entry(x).or_insert(0) += 1);
    let ans_part_2: u32 = all_line_points_counter.values().fold(0, |acc, x| acc + if *x > 1 {1} else {0});

    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: number of overlapping points on horizontal and vertical lines: {}", ans_part_1);
    println!("Part 2: number of overlapping points on all lines: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_line_parse() {
        assert_eq!(Line::parse("0,9 -> 5,8"), Line { p: Point::new(0, 9), q: Point::new(5, 8) });
    }

    #[test]
    fn test_line_get_points() {
        // Point
        let line: Line = Line { p: Point::new(1, 2), q: Point::new(1, 2) };
        assert_eq!(line.get_points(), HashSet::from_iter([Point::new(1, 2)]));

        // horizontal line
        let line: Line = Line { p: Point::new(1, 2), q: Point::new(3, 2) };
        assert_eq!(line.get_points(), HashSet::from_iter([Point::new(1, 2), Point::new(2, 2), Point::new(3, 2)]));

        // horizontal line, points reversed
        let line: Line = Line { p: Point::new(3, 2), q: Point::new(1, 2) };
        assert_eq!(line.get_points(), HashSet::from_iter([Point::new(1, 2), Point::new(2, 2), Point::new(3, 2)]));

        // vertical line
        let line: Line = Line { p: Point::new(2, 1), q: Point::new(2, 3) };
        assert_eq!(line.get_points(), HashSet::from_iter([Point::new(2, 1), Point::new(2, 2), Point::new(2, 3)]));
    }

    #[test]
    fn test_part_1_givens() {
        let given_input = "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2";
        let (ans_part_1, ans_part_2) = main_quiet(Box::new(BufReader::new(given_input.as_bytes()))).unwrap();
        assert_eq!(ans_part_1, 5);
        assert_eq!(ans_part_2, 12);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        let (ans_part_1, ans_part_2) = main_quiet(f).unwrap();
        assert_eq!(ans_part_1, 5608);
        assert_eq!(ans_part_2, 20299);
    }
}
