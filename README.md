# Overview
Using the [Advent of Code 2021](https://adventofcode.com/2021) as an excuse to
get better at Rust.

Back at it after [my last attempt](https://gitlab.com/romanows/advent-of-code-2019).
I had a rudimentary-at-best understanding of Rust back in 2019 and it's only
gone downhill since then, so: reader beware.


# Run
After installing rust and cloning this repo, change directory into the project
root and run:
```sh
cargo run --bin day01 --release
```

...to see the answers for my data files printed to stdout.

There is some [generated documentation](https://romanows.gitlab.io/advent-of-code-2021).
